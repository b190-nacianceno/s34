// npm init
/*
	- triggering this command will prompt the user for different settings that will define the application
	- using this command will make a "package.json" in our repo
	- package.json tracks the version of our application, depending on the settings we have set. also, we can see the dependencies that we have installed inside of this file
*/

/*
	npm install express
		-after triggering this command, express will now be listed as a "dependency". this can be seen inside the "package.json" file under the "dependencies" property

		- installing any dependency using npm will result into creating "node_modules" folder and package-lock.json
			"node_modules" directory should be left on the local repository because some of the hosting sites will fail to load the repository once it found the "node_modules" inside the repo. another reasone where node_modules is left on the local repository is it takes too much time to commit
			"node_modules" is also where the dependencies needed files are stored.

		".gitignore" files, as the name suggests will tell the git what files are to be spared/"ignored" in terms of commiting and pushing.

		"npm install" - this command is used when there are available dependencies inside our "package.json" but are not yet installed inside the repo/project - this is useful whentrying to clone a repo from a git repository to our local repository
*/



// we need now the express module since in this dependency, it has already built in codes that will allow the dev to create a server in a breeze/easier
const express = require("express");

// express() - allows us to create our server using express
// simply put, the app variable is our server
const app = express();

// setting up port variable
const port = 3000;

// app.use lets the server to handle data from requests
// 'app.use(express.json());' - allows the app to read and handle json data types
// methods used from express middlewares
	// middleware - software that provide common services and capabilities for the application
app.use(express.json());
// "app.use(express.urlencoded({extended: true}))" allows the server to read data from forms
// by default, information received from the url can only be received as a string or an array
// but using extended:true for the argument allows us to receive information from other data types such as objects which we will use throughout our application
app.use(express.urlencoded({extended: true}));


// SECTION-ROUTES
// GET method
/*
	Express has methods corresponding to each HTTP methods
	the route below expects to receive a GET request at the base URI "/"
*/
app.get("/", (req, res) => {
	res.send("Hello World");
})

// Tells our server to listen to the port
// if the port is accessed, we can run the server,
// returns a message to confirm that the server is running
app.listen(port, () => console.log(`Server running at port: ${port}`));